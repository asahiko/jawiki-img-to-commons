var apiURL = "http://ja.wikipedia.org/w/api.php";
iForm = document.getElementById('inputform');
oForm = document.getElementById('outputform');
var filename = '';
var fileDescription = '';
var fileRevisions = 0;
fileSizes = new Array();
fileKiBs = new Array();
fileTimestamps = new Array();
fileUsers = new Array();
fileComments = new Array();
var pageRevisions = 0;
pageTimestamps = new Array();
pageFlags = new Array();
pageUsers = new Array();
pageSummaries = new Array();
var timezone = 'UTC';

$('#inputform').submit(function(b) {
	b.preventDefault();
	
	var txt_element = document.getElementById('txt_name');
	
	filename = txt_element.value;
	pagename = "ファイル:" + encodeURIComponent(txt_element.value);
	
	$.getJSON(apiURL, {action:'query',prop:'info',format:'json',titles:pagename,indexpageids:1}, function(json) {
		var pageid = json.query.pageids[0];
		
		$.getJSON(apiURL, {action:'query',prop:'imageinfo',format:'json',titles:pagename,iilimit:9,iiprop:'timestamp|user|comment|size'}, function(json) {
			var data = json.query.pages[pageid].imageinfo;
			if (data === undefined) {
				oForm.elements['output'].value = "\n　エラー：日本語版ウィキペディアに存在するファイル名を入力してください。"; 
			} else {
				fileRevisions = data.length;
				for (i=0; i<fileRevisions; i++) {
					fileKiBs[i] = Math.round(data[i].size / 1024);
					fileSizes[i] = data[i].width + ' &times; ' + data[i].height + ' (' + fileKiBs[i] +' KiB)';
					fileTimestamps[i] = data[i].timestamp;
					fileUsers[i] = data[i].user;
					fileComments[i] = data[i].comment;
				}
				
				$.getJSON(apiURL, {action:'query',prop:'revisions',format:'json',titles:pagename,rvprop:'user|comment|timestamp|flags',rvlimit:19}, function(json) {
					var data = json.query.pages[pageid].revisions;
					if ( data === undefined ) {
						oForm.elements['output'].value = "\n　エラー：コモンズから呼び出されている（日本語版ローカルでない）ファイルではありませんか？"; 
					} else {
						pageRevisions = data.length;
						for (i=0; i<pageRevisions; i++) {
							detectFlags(data[i].minor);
							pageTimestamps[i] = data[i].timestamp;
							pageFlags[i] = detectFlags(data[i].minor);
							pageUsers[i] = data[i].user;
							pageSummaries[i] = data[i].comment;
						}
						
						compileTemplate();
					}
				});
			}
		});
	});
});

function compileTemplate() {
	var out = "";
	
	out += "{{Moved from Japanese Wikipedia\n";
	out += "|filename = " + filename + "\n";
	out += "|description = <pre>" + fileDescription + "</pre>\n";

	out += "|file_history = {{Moved from Japanese Wikipedia/FileHistory\n";
	for (i=0; i<fileRevisions; i++) {
		out += "  |datetime" + String(i+1) + " = " + fileTimestamps[i] + "\n";
		out += "  |demensions" + String(i+1) + " = " + fileSizes[i] + "\n";
		out += "  |user" + String(i+1) + " = " + fileUsers[i] + "\n";
		out += "  |comment" + String(i+1) + " = ''<nowiki>" + fileComments[i] + "</nowiki>''\n";
	}
	out += "}} \n";
	
	out += "|page_history = {{Moved from Japanese Wikipedia/PageHistory\n";
	for (i=0; i<pageRevisions; i++) {
		out += "  |datetime" + String(i+1) + " = " + pageTimestamps[i] + "\n";
		out += "  |flag" + String(i+1) + " = " + pageFlags[i] + "\n";
		out += "  |user" + String(i+1) + " = " + pageUsers[i] + "\n";
		out += "  |summary" + String(i+1) + " = ''<nowiki>" + pageSummaries[i] + "</nowiki>''\n";
	}
	out += "}} \n";
	out += "|other_information = \n"
	out += "}} \n";
	
	oForm.elements['output'].value = out ;
}

function detectFlags(minor) {
	if (minor === "") {
		return "m";
	} else {
		return "";
	}
}
